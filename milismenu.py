import gi, subprocess
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, GdkPixbuf, Gio, GtkLayerShell, Gdk, GLib

icon_text = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="64"
   height="64"
   version="1"
   id="svg12"
   sodipodi:docname="System.svg"
   inkscape:version="0.92.4 (5da689c313, 2019-01-14)">
  <metadata
     id="metadata18">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <defs
     id="defs16" />
  <sodipodi:namedview
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1"
     objecttolerance="10"
     gridtolerance="10"
     guidetolerance="10"
     inkscape:pageopacity="0"
     inkscape:pageshadow="2"
     inkscape:window-width="1908"
     inkscape:window-height="1044"
     id="namedview14"
     showgrid="false"
     inkscape:zoom="3.6875"
     inkscape:cx="-14.915254"
     inkscape:cy="32"
     inkscape:window-x="4"
     inkscape:window-y="28"
     inkscape:window-maximized="0"
     inkscape:current-layer="svg12" />
  <path
     style="opacity:0.2"
     d="M 30.599609,5 C 27.497209,5 25,7.4972093 25,10.599609 v 2.625 a 21,21 0 0 0 -6.623047,3.826172 l -2.275391,-1.3125 c -2.686757,-1.5512 -6.09919,-0.635976 -7.6503901,2.050781 l -1.4003907,2.421876 c -1.5511999,2.686758 -0.6359758,6.09919 2.0507813,7.65039 l 2.2656255,1.308594 A 21,21 0 0 0 11,33 a 21,21 0 0 0 0.367188,3.830078 l -2.2656255,1.308594 c -2.6867571,1.5512 -3.6019813,4.963633 -2.0507813,7.65039 l 1.4003907,2.421876 c 1.5512001,2.686757 4.9636331,3.601981 7.6503901,2.050781 l 2.259766,-1.304688 A 21,21 0 0 0 25,52.769531 v 2.63086 C 25,58.502791 27.497209,61 30.599609,61 h 2.800782 C 36.502791,61 39,58.502791 39,55.400391 v -2.640625 a 21,21 0 0 0 6.623047,-3.810547 l 2.275391,1.3125 c 2.686757,1.5512 6.09919,0.635977 7.65039,-2.050781 l 1.400391,-2.421876 c 1.5512,-2.686757 0.635975,-6.09919 -2.050781,-7.65039 L 52.632812,36.830078 A 21,21 0 0 0 53,33 21,21 0 0 0 52.632812,29.169922 l 2.265626,-1.308594 c 2.686757,-1.5512 3.601981,-4.963632 2.050781,-7.65039 l -1.400391,-2.421876 c -1.5512,-2.686758 -4.963633,-3.601981 -7.65039,-2.050781 l -2.259766,1.304688 A 21,21 0 0 0 39,13.228516 V 10.599609 C 39,7.4972093 36.502791,5 33.400391,5 Z"
     id="path2" />
  <path
     style="fill:#546e7a"
     d="M 30.599609,4 C 27.497209,4 25,6.4972094 25,9.5996094 v 2.6249996 a 21,21 0 0 0 -6.623047,3.826172 l -2.275391,-1.3125 c -2.686757,-1.5512 -6.09919,-0.635976 -7.6503901,2.050781 l -1.4003907,2.421876 c -1.5511999,2.686758 -0.6359758,6.09919 2.0507813,7.65039 l 2.2656255,1.308594 A 21,21 0 0 0 11,32 a 21,21 0 0 0 0.367188,3.830078 l -2.2656255,1.308594 c -2.6867571,1.5512 -3.6019813,4.963633 -2.0507813,7.65039 l 1.4003907,2.421876 c 1.5512001,2.686757 4.9636331,3.601981 7.6503901,2.050781 l 2.259766,-1.304688 A 21,21 0 0 0 25,51.769531 v 2.63086 C 25,57.502791 27.497209,60 30.599609,60 h 2.800782 C 36.502791,60 39,57.502791 39,54.400391 v -2.640625 a 21,21 0 0 0 6.623047,-3.810547 l 2.275391,1.3125 c 2.686757,1.5512 6.09919,0.635976 7.65039,-2.050781 l 1.400391,-2.421876 c 1.5512,-2.686757 0.635975,-6.09919 -2.050781,-7.65039 L 52.632812,35.830078 A 21,21 0 0 0 53,32 21,21 0 0 0 52.632812,28.169922 l 2.265626,-1.308594 c 2.686757,-1.5512 3.601981,-4.963632 2.050781,-7.65039 l -1.400391,-2.421876 c -1.5512,-2.686758 -4.963634,-3.601981 -7.65039,-2.050781 l -2.259766,1.304688 A 21,21 0 0 0 39,12.228516 V 9.5996094 C 39,6.4972094 36.502791,4 33.400391,4 Z"
     id="path4" />
  <path
     style="opacity:0.1;fill:#ffffff"
     d="M 30.599609,4 C 27.497209,4 25,6.4972094 25,9.5996094 V 10.599609 C 25,7.4972094 27.497209,5 30.599609,5 h 2.800782 C 36.502791,5 39,7.4972094 39,10.599609 V 9.5996094 C 39,6.4972094 36.502791,4 33.400391,4 Z M 25,12.224609 a 21,21 0 0 0 -6.623047,3.826172 l -2.275391,-1.3125 c -2.686757,-1.5512 -6.09919,-0.635976 -7.6503901,2.050781 l -1.4003907,2.421876 c -0.6033566,1.045044 -0.8286088,2.200037 -0.7265624,3.3125 0.069736,-0.789767 0.3027074,-1.578362 0.7265624,-2.3125 l 1.4003907,-2.421876 c 1.5512001,-2.686757 4.9636331,-3.601981 7.6503901,-2.050781 l 2.275391,1.3125 A 21,21 0 0 1 25,13.224609 Z m 14,0.0039 v 1 a 21,21 0 0 1 6.638672,3.814453 l 2.259766,-1.304688 c 2.686757,-1.5512 6.09919,-0.635977 7.65039,2.050781 l 1.400391,2.421876 c 0.423855,0.734138 0.656827,1.522733 0.726562,2.3125 0.102046,-1.112463 -0.123206,-2.267456 -0.726562,-3.3125 l -1.400391,-2.421876 c -1.5512,-2.686758 -4.963633,-3.601981 -7.65039,-2.050781 l -2.259766,1.304688 A 21,21 0 0 0 39,12.228516 Z M 52.767578,29.09179 52.632812,29.16992 A 21,21 0 0 1 52.974609,32.541016 21,21 0 0 0 53,32 21,21 0 0 0 52.767578,29.091797 Z m -41.529297,0.0039 A 21,21 0 0 0 11,32 a 21,21 0 0 0 0.02539,0.458984 21,21 0 0 1 0.341797,-3.289062 z m 41.523438,6.808594 a 21,21 0 0 1 -0.128907,0.925781 l 2.265626,1.308594 c 1.641712,0.947843 2.617034,2.590267 2.777343,4.33789 0.185479,-2.100574 -0.824725,-4.210545 -2.777343,-5.33789 z m -41.529297,0.0039 -2.1308595,1.230469 c -1.9526187,1.127345 -2.9628223,3.237316 -2.7773437,5.33789 0.1603097,-1.747623 1.1356309,-3.390047 2.7773437,-4.33789 l 2.2656255,-1.308594 a 21,21 0 0 1 -0.134766,-0.921875 z"
     id="path6" />
  <path
     style="opacity:0.2"
     d="m 32.000237,21.00088 c 6.627221,0 11.999676,5.372455 11.999676,11.999676 0,6.627221 -5.372455,11.999676 -11.999676,11.999676 -6.627221,0 -11.999676,-5.372455 -11.999676,-11.999676 0,-6.627221 5.372455,-11.999676 11.999676,-11.999676 z"
     id="path8" />
  <path
     style="fill:#ffffff"
     d="m 32.000237,20.001334 c 6.627221,0 11.999676,5.372455 11.999676,11.999676 0,6.627221 -5.372455,11.999676 -11.999676,11.999676 -6.627221,0 -11.999676,-5.372455 -11.999676,-11.999676 0,-6.627221 5.372455,-11.999676 11.999676,-11.999676 z"
     id="path10" />
</svg>"""


settings = {"size_width":640,
            "size_height":480,
            "icon_size":36,
            "show_generic_name":False,
            "show_desc":False,
            "horizontal":True,
            "shutdown_command":"sudo poweroff",
            "reboot_command":"sudo reboot",
            "logout_command":"sudo logout"}

class MilisMenu(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)
        main_box = Gtk.VBox()
        self.add(main_box)

        self.all_apps = Gio.DesktopAppInfo.get_all()
        self.icon_theme = Gtk.IconTheme.get_default()

        s_box = Gtk.HBox()
        main_box.pack_start(s_box,False,False,5)
        self.menu_entry = Gtk.SearchEntry()
        self.menu_entry.connect("search-changed",self.menu_entry_change)
        s_box.pack_start(self.menu_entry,True,True,10)

        logout_btn = Gtk.Button()
        logout_btn.connect("clicked",self.run_command,settings["logout_command"])
        pb =  self.get_icon_in_theme("xfsm-logout",24)
        image = Gtk.Image.new_from_pixbuf(pb)
        logout_btn.set_image(image)
        s_box.pack_start(logout_btn,False,False,10)

        reboot_btn = Gtk.Button()
        reboot_btn.connect("clicked",self.run_command,settings["reboot_command"])
        pb =  self.get_icon_in_theme("xfsm-reboot",24)
        image = Gtk.Image.new_from_pixbuf(pb)
        reboot_btn.set_image(image)
        s_box.pack_start(reboot_btn,False,False,10)

        close_btn = Gtk.Button()
        close_btn.connect("clicked",self.run_command,settings["shutdown_command"])
        pb =  self.get_icon_in_theme("xfsm-shutdown",24)
        image = Gtk.Image.new_from_pixbuf(pb)
        close_btn.set_image(image)
        s_box.pack_start(close_btn,False,False,10)


        s_box = Gtk.HBox()
        self.apps_iw_store = Gtk.ListStore(str,GdkPixbuf.Pixbuf,Gio.DesktopAppInfo)
        self.apps_iw = Gtk.IconView(model=self.apps_iw_store)
        self.apps_iw.connect("item-activated",self.apps_iw_activate)
        if settings["horizontal"]:
            self.apps_iw.set_item_orientation(Gtk.Orientation.HORIZONTAL)
        self.apps_iw.set_text_column(0)
        self.apps_iw.set_pixbuf_column(1)
        scroll = Gtk.ScrolledWindow()
        scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        scroll.add(self.apps_iw)
        s_box.pack_start(scroll,True,True,10)
        main_box.pack_start(s_box,True,True,5)

        self.apps_store_up("")
        self.connect("key-press-event",self.key_press)
        self.set_default_size(settings["size_width"],settings["size_height"])

    def run_command(self,widget,command):
        subprocess.Popen(command.split())
        self.destroy()


    def key_press(self, widget, event):
        key_name = Gdk.keyval_name(event.keyval)
        arrow_keys = ["Up","Down","Left","Right"]
        if key_name in arrow_keys:
            self.apps_iw.grab_focus()
        elif key_name == "Return":
            if len(self.apps_iw.get_selected_items()) != 0:
                self.apps_iw_activate(self.apps_iw, self.apps_iw.get_selected_items()[0])
        elif key_name == "Escape":
            self.destroy()
        else:
            if not self.menu_entry.is_focus():
                self.menu_entry.grab_focus()


    def apps_iw_activate(self,widget,path):
        model = widget.get_model()
        app = model[path][2]
        app.launch(None,None)
        self.destroy()

    def apps_store_up(self,search=""):
        self.apps_iw_store.clear()
        for app in self.all_apps:
            name = app.get_name()
            desc = app.get_description()
            g_name = app.get_generic_name()
            if name == None:
                name = ""
            if desc == None:
                desc = ""
            if search == "" or search.lower() in name.lower() or search.lower() in desc.lower():
                if settings["show_desc"] and desc != None:
                    name += "\n"
                    name += desc
                if settings["show_generic_name"] and g_name != None:
                    name += "\n"
                    name += g_name
                icon_names = [name,name.lower(),app.get_executable()]
                icon = app.get_icon()
                if icon != None:
                    icon_string = icon.to_string()
                    icon_names.insert(0,icon_string)
                for n in icon_names:
                    icon = self.get_icon_in_theme(n)
                    if icon:
                        break
                if icon:
                    self.apps_iw_store.append([name,icon,app])
                else:
                    loader = GdkPixbuf.PixbufLoader()
                    loader.set_size(settings["icon_size"],settings["icon_size"])
                    loader.write(icon_text.encode())
                    loader.close()
                    image = loader.get_pixbuf()
                    #image = GdkPixbuf.Pixbuf.new_from_file_at_size("./icons/default_app.svg",settings["icon_size"],settings["icon_size"])
                    self.apps_iw_store.append([name, image,app])
        self.set_iw_select_item(self.apps_iw,0)

    def set_iw_select_item(self,iw,index):
        try:
            model = iw.get_model()
            if model != None:
                iter = model[index]
                iw.select_path(iter.path)
        except:
            pass

    def get_icon_in_theme(self, icon_name, icon_size = False):
        if not icon_size:
            icon_size = settings["icon_size"]
        try:
            icon = self.icon_theme.load_icon(icon_name,
                                             icon_size,
                                             Gtk.IconLookupFlags.FORCE_SIZE)
            return icon
        except:
            return False

    def menu_entry_change(self,widget):
        text = widget.get_text().lower()
        self.apps_store_up(text)


screen = Gdk.Display.get_default()
monitors = screen.get_n_monitors()
active_monitor = screen.get_monitor(0)
SIZE = active_monitor.get_geometry()
print(SIZE.width,SIZE.height)

w_space = (SIZE.width - settings["size_width"]) // 2
h_space = (SIZE.height - settings["size_height"]) // 2

print(w_space, h_space)

if __name__ == '__main__':	
    win = MilisMenu()
    GtkLayerShell.init_for_window(win)
    GtkLayerShell.auto_exclusive_zone_enable(win)
    GtkLayerShell.set_margin(win, GtkLayerShell.Edge.TOP, h_space)
    GtkLayerShell.set_margin(win, GtkLayerShell.Edge.BOTTOM, h_space)
    GtkLayerShell.set_margin(win, GtkLayerShell.Edge.LEFT, w_space)
    GtkLayerShell.set_margin(win, GtkLayerShell.Edge.RIGHT, w_space)
    GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.TOP, 1)
    GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.BOTTOM, 1)
    GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.LEFT, 1)
    GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.RIGHT, 1)
    GtkLayerShell.set_keyboard_mode(win, GtkLayerShell.KeyboardMode.EXCLUSIVE)
    win.show_all()
    win.connect('destroy', Gtk.main_quit)
    Gtk.main()